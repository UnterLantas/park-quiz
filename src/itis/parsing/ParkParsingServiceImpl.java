package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.ArrayList;

public class ParkParsingServiceImpl implements ParkParsingService {

    ArrayList<String> arrayList = new ArrayList<>();
    String ph;
    boolean flag = true;

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException {
        arrayList = null;
        ph = null;
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(parkDatafilePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        assert fileReader != null;
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        Field[] fields = Park.class.getDeclaredFields();
        while (true) {
            try {
                if ((ph = bufferedReader.readLine()).equals("***")) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            arrayList.add(ph);
        }
        try {
            fileReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (arrayList != null) {
            for (String s : arrayList) {
                for (Field field : fields) {
                    flag = true;
                    FieldName fieldName = field.getAnnotation(FieldName.class);
                    if (fieldName != null) {
                        flag = false;
                        String anno = fieldName.value();
                        if (anno.equals(s.substring(1, s.indexOf(':') - 2))) {
                            s = s.substring(s.indexOf(':'));
                            String theName = s.substring(s.indexOf('"'), s.indexOf('"' - 1));
                            flag = true;
                        }
                    }
                    MaxLength maxLength = field.getAnnotation(MaxLength.class);
                    if (maxLength != null) {
                        flag = false;
                        int max = maxLength.value();
                        s = s.substring(s.indexOf(':'));
                        int ownerInn = Integer.parseInt(s.substring(s.indexOf('"'), s.indexOf('"') - 1));
                        if (ownerInn <= max) {
                            flag = true;
                        }
                    }
                    NotBlank notBlank = field.getAnnotation(NotBlank.class);
                    if (notBlank != null) {
                        flag = false;
                        String ph1 = s.substring(s.indexOf(':'));
                        if (!ph1.substring(ph1.indexOf('"')).equals("null")) {
                            if (s.substring(1, s.indexOf('"') - 1).equals("foundationYear")) {
                                s = s.substring(s.indexOf(':'));
                                LocalDate parse = LocalDate.parse(s.substring(s.indexOf('"'), s.indexOf('"') - 1));
                                flag = true;
                            }
                        }

                    }
                }
                if (flag) {
                    Class<?> c = Park.class;
                    try {
                        Constructor<?> constructor = c.getDeclaredConstructor(String.class, int.class, LocalDate.class);
                        constructor.setAccessible(true);
                        Park park = (Park) constructor.newInstance();
                        return park;
                    } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }


        }
        return null;
    }
}
